import {Books} from './models/books';

export const BOOKLIST: Books[] = [
  {
    id: 1,
    title: 'GOT - Le trone de fer',
    description: 'Comme la plupart des récits de fantasy, Le Trône de fer est une histoire pleine de bruit et de fureur, où s\'expriment la passion absolue du pouvoir et ses corollaires : le mensonge, la trahison et, au final, la violence et le meurtre. Dans ce monde médiéval, un mur gigantesque sépare le nord du royaume des terres aux forêts anormalement glaciales, hantées, selon les vieilles légendes, de loups-garous et de créatures fantomatiques. Mais le roi Robert a d\'autres motifs d\'inquiétude : des désertions minent sa légendaire Garde de nuit, victime d\'embuscades, et on conspire contre lui. Le duc Stark, son fidèle allié, va donc devoir quitter ses terres pour rejoindre la Cour. Car derrière les grands mots d\'honneur et de loyauté, des complots se nouent…',
    img: 'https://image.noelshack.com/fichiers/2019/17/4/1556174244-test2.jpg'
  },
  {
    id: 2,
    title: 'GOT - Le donjon rouge',
    description: 'Comment Lord Eddard Stark, seigneur de Winterfell, Main du Roi, gravement blessé par traîtrise, et par la même plus que jamais à la merci de la perfide reine Cersei ou des imprévisibles caprices du despotique roi Robert, aurait-il une chance déchapper à la nasse tissée dans lombre pour labattre ? Comment, armé de sa seule et inébranlable loyauté, cerné de toutes parts par dabominables intrigues, pourrait-il à la fois survivre, sauvegarder les siens et assurer la pérennité du royaume ? Comment ne serait-il pas voué à être finalement broyé dans un engrenage infernal, alors que Catelyn, son épouse, a mis le feu aux poudres en semparant du diabolique nain Tyrion, le frère de la reine ? Ce modèle est livré par le fabricant sous forme d\'un assortiment aléatoire de plusieurs modèles et/ou coloris. Il nous est donc impossible de vous proposer un modèle et/ou un coloris en particulier. En validant votre commande, vous recevrez donc un des modèles figurant sur l\'image en fonction du stock disponible. Nous vous remercions pour votre compréhension.',
    img: 'https://image.noelshack.com/fichiers/2019/17/4/1556174412-test3.jpg'
  },
  {
    id: 3,
    title: 'GOT - Intégrale 3',
    description: 'Dans cette troisième intégrale, George R.R. Martin continue de nous entraîner dans un monde fabuleux où les familles de ses héros se ramifient au cœur de régions plus mystérieuses les unes que les autres : grottes, collines creuses, hameau de feuilles, forteresses imprenables. Ici entrent également en scène des monstres terrifiants, esclaves de forces maléfiques qui n’ont qu’un but sur terre : éradiquer toute trace d’humanité. Odieuses mutilations, drames sanglants, mariages imposés, traîtres sans vergogne, vengeances cruelles longuement mûries, équipées punitives, se succèdent au fil de ces pages éblouissantes. Car rien n’arrête l’imagination foisonnante de George R.R. Martin qui poursuit là l’un des cycles romanesques et visionnaires les plus originaux de tous les temps.',
    img: 'https://image.noelshack.com/fichiers/2019/17/4/1556182872-test4.jpg'
  }
]
