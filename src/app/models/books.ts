export interface Books {
  id: number;
  title: string;
  description: string;
  img: string;
}
