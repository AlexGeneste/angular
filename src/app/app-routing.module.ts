import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NavbarComponent} from './navbar/navbar.component';
import {CarousselComponent} from './caroussel/caroussel.component';
import {AccueilComponent} from './accueil/accueil.component';
import {ListtomeComponent} from './listtome/listtome.component';
import {ContactusComponent} from './contactus/contactus.component';
import {BooksInfosComponent} from './books-infos/books-infos.component';

const routes: Routes = [
  {path: '', redirectTo: 'accueil', pathMatch: 'full'},
  {path: 'accueil', component: AccueilComponent },
  {path: 'listeTomes', component: ListtomeComponent},
  {path: 'contactUs', component: ContactusComponent},
  {path: 'book/:id', component: BooksInfosComponent},
  {path: '**', redirectTo: 'accueil'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
