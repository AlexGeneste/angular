import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListtomeComponent } from './listtome.component';

describe('ListtomeComponent', () => {
  let component: ListtomeComponent;
  let fixture: ComponentFixture<ListtomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListtomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListtomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
