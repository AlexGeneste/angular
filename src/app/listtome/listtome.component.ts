import { Component, OnInit } from '@angular/core';
import {BOOKLIST} from '../mock-books';
import {BookService} from '../services/book.service';

@Component({
  selector: 'lib-listtome',
  templateUrl: './listtome.component.html',
  styleUrls: ['./listtome.component.scss']
})
export class ListtomeComponent implements OnInit{
  bookList = BOOKLIST;

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
    this.getBookList();
  }

  getBookList(): void {
    this.bookService.getBookList()
      .subscribe(bookList => this.bookList = bookList);
  }
}
