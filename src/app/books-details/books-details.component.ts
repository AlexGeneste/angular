import {Component, Input, OnInit} from '@angular/core';
import {Books} from '../models/books';

@Component({
  selector: 'lib-books-details',
  templateUrl: './books-details.component.html',
  styleUrls: ['./books-details.component.scss']
})
export class BooksDetailsComponent {
  @Input() bookItem: Books;
}
