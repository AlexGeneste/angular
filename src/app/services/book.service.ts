import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Books} from '../models/books';
import {BOOKLIST} from '../mock-books';

@Injectable({providedIn: 'root'})
export class BookService {
  getBookList(): Observable<Books[]> {
    return of(BOOKLIST);
  }
  getBook(id: number): Observable<Books> {
    return of(BOOKLIST.find(book => book.id === id));
  }
}
