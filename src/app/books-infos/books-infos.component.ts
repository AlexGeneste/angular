import {Component, OnInit} from '@angular/core';
import {Books} from '../models/books';
import {ActivatedRoute} from '@angular/router';
import {BookService} from '../services/book.service';

@Component({
  selector: 'lib-books-infos',
  templateUrl: './books-infos.component.html',
  styleUrls: ['./books-infos.component.scss']
})
export class BooksInfosComponent implements OnInit {
  book: Books;

  constructor(
    private route: ActivatedRoute,
    private bookService: BookService,
  ) { }

  ngOnInit(): void {
    this.getBooks();
  }

  getBooks(): void {
    const id = + this.route.snapshot.paramMap.get('id');
    this.bookService.getBook(id).subscribe(book => this.book = book)
  }
}
