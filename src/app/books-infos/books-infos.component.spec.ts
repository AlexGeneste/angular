import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BooksInfosComponent } from './books-infos.component';

describe('BooksInfosComponent', () => {
  let component: BooksInfosComponent;
  let fixture: ComponentFixture<BooksInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooksInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
