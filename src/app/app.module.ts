import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CarousselComponent } from './caroussel/caroussel.component';
import { IgxCarouselModule, IgxSliderModule } from 'igniteui-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccueilComponent } from './accueil/accueil.component';
import { ListtomeComponent } from './listtome/listtome.component';
import { ContactusComponent } from './contactus/contactus.component';
import { BooksDetailsComponent } from './books-details/books-details.component';
import { BooksInfosComponent } from './books-infos/books-infos.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CarousselComponent,
    AccueilComponent,
    ListtomeComponent,
    ContactusComponent,
    BooksDetailsComponent,
    BooksInfosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    IgxCarouselModule,
    IgxSliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
